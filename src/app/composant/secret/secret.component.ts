import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-secret',
  templateUrl: './secret.component.html',
  styleUrls: ['./secret.component.scss']
})
export class SecretComponent implements OnInit {

  count: number = 0;
  clic: boolean = false;
  clicsDate : string[] = [];
  now: any;
  date: any;
  color: string = "text-dark";
  valid: boolean = false;

  constructor() { }

  displayText()
  {
    this.clic = true;
    this.count++;
    this.now = new Date; 
    this.date = `Le ${this.now.getDate()}/${this.now.getMonth()+1}/${this.now.getFullYear()} à ${this.now.getHours()}:${this.now.getMinutes()}:${this.now.getSeconds()}`;
    this.clicsDate.push(this.date);
    if(this.count>=5)
    {
      this.color = "text-light";
    }
  }

  ngOnInit(): void {
  }

}
